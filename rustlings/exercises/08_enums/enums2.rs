#[derive(Debug)]
struct Point {
    x: u64,
    y: u64,
}

#[derive(Debug)]
enum Message {
    Resize { width: u8, height: u8},
    Move(Point),
    Echo(String),
    ChangeColor(u8, u8, u8),
    Quit,
}

impl Message {
    fn call(&self) {
        println!("{self:?}");
        match self {
            Self::Resize { width, height} => {
                println!("Rezsize w: {}, h: {}", width, height)
            },
            Self::Move(p) => {
                println!("Move: {:?}", p)
            },
            Self::Echo(s) => {
                println!("{}", s)
            },
            Self::ChangeColor(r, g, b) => {
                println!("ChangeColor: r: {}, g: {}, b: {}", r, g, b)
            },
            Self::Quit => {
                println!("Quit")
            }
        }
    }
}

fn main() {
    let messages = [
        Message::Resize {
            width: 10,
            height: 30,
        },
        Message::Move(Point { x: 10, y: 15 }),
        Message::Echo(String::from("hello world")),
        Message::ChangeColor(200, 255, 255),
        Message::Quit,
    ];

    for message in &messages {
        message.call();
    }
}
